<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id','id');
    }
    
     public function review_replies(){
        return $this->hasMany(ReviewReply::class, 'review_id','id')->with('user');
    }
}
