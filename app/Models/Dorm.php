<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dorm extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function dorm_images()
    {
        return $this->hasMany(DormImage::class,'dorm_id','id');
    }
    
       public function reviews()
    {
        return $this->hasMany(Review::class,'dorm_id','id')->with('user','review_replies');
    }
    

}
