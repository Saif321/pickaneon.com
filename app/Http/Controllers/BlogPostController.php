<?php

namespace App\Http\Controllers;

use App\Models\{BlogComment,CommentReply};
use App\Models\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogPostController extends Controller
{
    
    public function add_new_blog_post(Request $request){

        $validated = $request->validate([
            'title' => 'required|unique:blog_posts|max:255',
            'body' => 'required',
        ]);

        // return $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = uniqid() . '.' . $image->getClientOriginalExtension();
            // Store the file in the storage path
            $storedFilePath = Storage::disk('public')->putFileAs('images', $image, $filename);
            $request['featured_image_url'] = $storedFilePath;
        }

        $blog = BlogPost::create($request->except('image'));

        if ($blog) {
            return response()->json([
                "status" => "200",
                "message" => "Post created successfully",
                "blog" => $blog,
            ]);
        }
    }

    public function get_all_blogs(Request $request){

        // return $request->all();

        $blog = BlogPost::with('comments')->get();

        if ($blog) {
            return response()->json([
                "status" => "200",
                "blog" => $blog,
            ]);
        } else {
            return response()->json([
                "status" => "404",
                "message" => "no post",
            ]);
        }
    }

    public function get_specific_blog(Request $request){

        $blog = BlogPost::where('id', $request->blog_id)->with('comments')->first();

        if ($blog) {
            return response()->json([
                "status" => "200",
                "blog" => $blog,
            ]);
        } else {
            return response()->json([
                "status" => "404",
                "message" => "no blog post",
            ]);
        }
    }

    public function edit_blog_post(Request $request){

        $validated = $request->validate([
            'blog_id' => 'required',
        ]);

        $blog = BlogPost::where('id', $request->blog_id)->first();
        if ($blog) {
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = uniqid() . '.' . $image->getClientOriginalExtension();
                // Store the file in the storage path
                $storedFilePath = Storage::disk('public')->putFileAs('images', $image, $filename);
                $blog->update([
                    'featured_image_url' => $storedFilePath,
                ]);
            }
            if ($request->title) {
                $blog->update([
                    'title' => $request->title,
                ]);
            }
            if ($request->body) {
                $blog->update([
                    'body' => $request->body,
                ]);
            }
            return response()->json([
                'status' => "200",
                "message" => "blog updated successfully",
                "blog"=>$blog,
            ]);
        } else {
            return response()->json([
                'status' => "404",
                "message" => "blog not found",
            ]);
        }
    }
    
    public function delete_blog_post(Request $request){
        $validated = $request->validate([
            'blog_id' => 'required',
        ]);
        $blog = BlogPost::where('id', $request->blog_id)->delete();

        if ($blog) {
            return response()->json([
                'status' => "200",
                "message" => "blog deleted successfully",
            ]);
        } else {
            return response()->json([
                'status' => "404",
                "message" => "blog not found",
            ]);
        }
    }
    
    public function post_blog_comment(Request $request){
         $validated = $request->validate([
        'blog_id' => 'required',
        'comment' => 'required',
        
                 ]);
        $comment = BlogComment::create([
            'user_id'=>auth()->user()->id,
            'blog_id'=>$request->blog_id,
            'comment'=>$request->comment,
            'rating'=>$request->rating,
        ]);
        
        return response()->json([
                'status'=>200,
                'message'=>'comment posted successfully',
                'comment'=>$comment,
                'user'=>auth()->user(),
            
            ]);
        
    }
    
    public function get_blog_comment(Request $request,$id){
    $dorm_comments = BlogComment::where('blog_id',$id)->with('user')->get();
        return response()->json([
            'status'=>200,
            'comments'=>$dorm_comments,
        ]);
    }
    
    public function blog_reply(Request $request){
         $validated = $request->validate([
            'comment_id' => 'required',
            'reply'=>'required',
        ]);
        
        $reply = CommentReply::create([
                'comment_id' =>  $request->comment_id,
                'reply' =>  $request->reply,
                'user_id' =>  auth()->id(),
            ]);
        return response()->json([
            'status'=>200,
            'message' => "comment added successfully",
            'reply' => $reply,
            ]);
    }

}
